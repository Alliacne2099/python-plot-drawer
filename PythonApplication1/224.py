import numpy as np
import sys # Для sys.exit()
import matplotlib.pyplot as plt
import math

img_rc = 64		# size of image square in pixes
N = 10			# number of images in each class
N_CLASSES = 6	

imgs = np.zeros((N * N_CLASSES, img_rc, img_rc), dtype = 'uint8')

##img =imgs[0]
##plt.imshow(img, cmap='gray')
##plt.show()
all_a = []
all_b = []


def rnd2():
	return np.random.choice([-1, 0, 1])

def rndcol():
	return np.random.choice(range(70, 255))

def drawline(i, x0, y0, x1, y1, steps):
	t = 0
	dt = 1 / steps
	while t < 1:
		t += dt
		x = x0 * (1 - t) + x1 * t
		y = y0 * (1 - t) + y1 * t
		imgs[i, int(y) + rnd2(), int(x) + rnd2()] = rndcol()

def sampleFn(t):
	return 32 + math.sin(t * math.pi), 32 + math.cos(t * math.pi)

def drawPlot(i, fn, neg=False):
	if(neg):
		def negfn(t):
			return fn(-t)
		drawPlot(i, negfn)
	t = 0
	x, y = fn(t)
	xi = 32 + int(x) + rnd2()
	yi = 32 - int(y) + rnd2()
	if(xi < 0 or xi > 63 or yi < 0 or yi > 63):
		return
	imgs[i, yi, xi] = rndcol()

	dt = 0.01
	x1, y1 = fn(t + dt)
	dl = math.sqrt((x1 - x) * (x1 - x) + (y1 - y) * (y1 - y)) # = 1/3
	dt = dt * (1 / 3 / dl)

	while t <= 1:
		x1, y1 = fn(t + dt)
		dl = math.sqrt((x1 - x) * (x1 - x) + (y1 - y) * (y1 - y)) # = 1/3
		dt = dt * (1 / 3 / dl)
		t = t + dt

		x, y = fn(t)
		xi = 32 + int(x) + rnd2()
		yi = 32 - int(y) + rnd2()
		if(xi < 0 or xi > 63 or yi < 0 or yi > 63):
			break
		#print(i, t, xi, yi)
		imgs[i, yi, xi] = rndcol()


	
##Ромб и ромб без одной стороны.  Варьируются длина стороны ромба и угол ромба.
##	Удаляемая сторона выбирается случайным образом.
class_id = 0
for i in range(class_id * N, class_id * N + N):
	print('drawing plots #' + str(i + 1) + ' & #' + str(i + N + 1) + ' of ' + str(N_CLASSES * N) + '\r',end='\r')

	a = np.random.uniform(math.pi / 2 * 0.2, math.pi / 2 * 0.8)
	b = np.random.uniform(15., 30.)
	
	x0 = math.sin(a) * b
	y0 = math.cos(a) * b
	
	drawline(i, 32, 32 + y0, 32 + x0, 32, b * 3)
	drawline(i, 32, 32 + y0, 32 - x0, 32, b * 3)
	drawline(i, 32, 32 - y0, 32 + x0, 32, b * 3)
	drawline(i, 32, 32 - y0, 32 - x0, 32, b * 3)

	a = np.random.uniform(math.pi / 2 * 0.2, math.pi / 2 * 0.8)
	b = np.random.uniform(15., 30.)
	
	x0 = math.sin(a) * b
	y0 = math.cos(a) * b
	
	side = np.random.choice([1,2,3,4])
	if side != 1:
		drawline(N + i, 32, 32 + y0, 32 + x0, 32, b * 3)
	if side != 2:
		drawline(N + i, 32, 32 + y0, 32 - x0, 32, b * 3)
	if side != 3:
		drawline(N + i, 32, 32 - y0, 32 + x0, 32, b * 3)
	if side != 4:
		drawline(N + i, 32, 32 - y0, 32 - x0, 32, b * 3)

	
##Гипербола y = a/x и одна часть гиперболы.  Варьируются коэффициент a,
##	знак этого коэффициента и случайным образом выбирается часть гиперболы с y >
##	0 или y < 0.
class_id = 2
for i in range(class_id * N, class_id * N + N):
	print('drawing plots #' + str(i + 1) + ' & #' + str(i + N + 1) + ' of ' + str(N_CLASSES * N) + '\r',end='\r')
	
	def hyper1(t):
		return a * (1 - t), a / (1 - t)
	def hyper2(t):
		return a / (1 - t), a * (1 - t)

	def hyper3(t):
		return -a * (1 - t), -a / (1 - t)
	def hyper4(t):
		return -a / (1 - t), -a * (1 - t)

	a = np.random.uniform(10., 200.)
	a = math.sqrt(a)
	
	drawPlot(i, hyper1)
	drawPlot(i, hyper2)
	drawPlot(i, hyper3)
	drawPlot(i, hyper4)
	
	a = np.random.uniform(10., 200.)
	a = math.sqrt(a)
	side = np.random.choice([1,2])
	
	if side == 1:
		drawPlot(N + i, hyper1)
		drawPlot(N + i, hyper2)
	if side == 2:
		drawPlot(N + i, hyper3)
		drawPlot(N + i, hyper4)
	


##Прямоугольник и прямоугольник без одной стороны.  Варьируются длины стороны
##прямоугольника
##	и угол поворота прямоугольника относительно его центра.  Удаляемая
##	сторона выбирается случайным образом.
class_id = 4
for i in range(class_id * N, class_id * N + N):
	print('drawing plots #' + str(i + 1) + ' & #' + str(i + N + 1) + ' of ' + str(N_CLASSES * N) + '\r',end='\r')

	def rotate(v, a):
		x, y = v
		return x * math.sin(a) + y * math.cos(a),-x * math.cos(a) + y * math.sin(a)

	def rect1(t):
		return rotate((a,b * t,), angle)
	def rect2(t):
		return rotate((b,a * t,), angle + math.pi / 2)
	def rect3(t):
		return rotate((a,b * t,), angle + math.pi)
	def rect4(t):
		return rotate((b,a * t,), angle - math.pi / 2)
	
	a = np.random.uniform(5., 20.)
	b = np.random.uniform(5., 20.)
	angle = np.random.uniform(0., 2 * math.pi)
	
	drawPlot(i, rect1, True)
	drawPlot(i, rect2, True)
	drawPlot(i, rect3, True)
	drawPlot(i, rect4, True)


	a = np.random.uniform(5., 20.)
	b = np.random.uniform(5., 20.)
	angle = np.random.uniform(0., 2 * math.pi)
	
	drawPlot(N + i, rect1, True)
	drawPlot(N + i, rect2, True)
	drawPlot(N + i, rect3, True)


	
for i in range(N_CLASSES):					# 6 rows
	for j in range(10):						# 10 imgs each
		plt.subplot(6, 10, i * 10 + j + 1) 	# inserting to rows
		img = imgs[i * N + j]				# first 10 items from list
		plt.imshow(img, cmap='gray')
		plt.axis('off')
		#ttl = all_a[i] + ';' + all_b[i]
		#plt.title(ttl,{'fontsize':7,'verticalalignment':'bottom'})
plt.subplots_adjust(hspace = 0.15)
plt.show()

imgs_labels = np.fromfunction(np.vectorize(lambda i: i // N), (N_CLASSES * N,), dtype='int8')
# print(str(imgs_labels))
path = 'D:\\Public\\python\\'
fn = path + 'data.bin'
fn2 = path + 'labels.bin'
file_data = open(fn, 'wb')
file_labels = open(fn2, 'wb')
file_data.write(imgs)
file_labels.write(imgs_labels)
file_data.close()
file_labels.close()

