import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib import cm
# from mpl_toolkits.mplot3d import Axes3D

#матрица формы пластины
def Matr(N, M):
    matr = np.zeros((3*N, 3*M))
    for i in range(1, 3*N - 1):
        for j in range(1, M - 1):
            matr[i][j] = 1
    for i in range(N + 1, 2*N - 1):
        for j in range(M - 1, 3*M - 1):
            matr[i][j] = 1
    for i in range(0, 3*N):
        matr[i][0] = 2
        if (i <= N) | (i > 2 * N - 1):
            matr[i][M - 1] = 2
        if (i >= N) & (i <=2 * N - 1):
            matr[i][3 * M - 1] = 2
    for i in range(1, 3*M):
        if i >= M - 1:
            matr[N][i] = 2
            matr[2 * N - 1][i] = 2
        elif i <= M:
            matr[0][i] = 2
            matr[3 * N - 1][i] = 2
    return matr

#копирование слоя
def copy(nm, n, m):
    mat = np.zeros((3*n, 3*m))
    for i in range(0, 3*n):
        for j in range(0, 3*m):
            mat[i][j] = nm[i][j]
    return mat

#аналитическая функция
def u(x, y, t):
    return math.sin(math.pi * x) * math.sin(math.pi * y) * math.exp(-2 * math.pi**2 * t) + (2*x + 5*y) * t

#функция распространения тепла
def fi(x, y):
    return math.sin(math.pi * x) * math.sin(math.pi * y)

#гранца
def g(x, y, t):
    return u(x, y, t)

#функция из условия
def f(x, y ,t):
    return 2*x + 5*y

#оператор Лапласа
def L(i, j, hx, hy, sl):
    return ((sl[i+1][j] + sl[i-1][j] - 2*sl[i][j])/hx**2 + (sl[i][j+1] + sl[i][j-1] - 2*sl[i][j])/hy**2)

#построение явной схемы
def sloi(n, m, tau):
    sl = np.zeros((tau, 3*n, 3*m))
    prom = np.zeros((3*n, 3*m))
    hx = 1 / (3 * n)
    hy = 1 / (3 * m)
    ht = 1 / tau
    matr = Matr(n, m)
    for k in range(0, tau):
        if k == 0:
            for i in range(0, 3 * n):
                for j in range(0, 3 * m):
                    if matr[i][j] == 1:
                        sl[k][i][j] = fi(i * hx, j * hy)
                    if matr[i][j] == 2:
                        sl[k][i][j] = g(i * hx, j * hy, k * ht)
        else:
            for i in range(0, 3 * n):
                for j in range(0, 3 * m):
                    if matr[i][j] == 1:
                        sl[k][i][j] = prom[i][j] + ht * f(i * hx, j * hy, k * ht) + ht * L(i, j, hx, hy, prom)
                    if matr[i][j] == 2:
                        sl[k][i][j] = g(i * hx, j * hy, k * ht)
        prom = copy(sl[k], n, m)
    return sl

#поиск точного решения
def tochn(n, m, t):
    m3 = np.zeros((t, 3*n, 3*m))
    hx = 1 / (3 * n)
    hy = 1 / (3 * m)
    ht = 1 / t
    matr = Matr(n, m)
    for k in range(0, t):
        for i in range(0, 3 * n):
            for j in range(0, 3 * m):
                if matr[i][j] != 0:
                    m3[k][i][j] = u(i * hx, j * hy, k * ht)
    return m3

#максимальная разность на слое
def raz(s1, s2, n, m):
    ret = -1
    for i in range(0, 3*n):
        for j in range(0, 3*m):
            ra = math.fabs(s1[i][j] - s2[i][j])
            if (ra > ret) | (ret == -1):
                ret = ra
    return ret

#погрешность решения
def pogr(s1, s2, n, m, t):
    ret = -1
    for k in range(0, t):
        ra = raz(s1[k], s2[k], n, m)
        if (ra > ret) | (ret == -1):
            ret = ra
    return ret

#погрешность по Рунге
def rung(s, n, m, t):
    st = sloi(n, m, int(t/2))
    ret = -1
    for k in range(0, int(t/2)):
        ra = raz(s[k*2], st[k], n, m) / 3
        if (ra > ret) | (ret == -1):
            ret = ra
    return ret

def save_in_file(filename, s, n, m):
    with open(filename, 'w') as f:
        for i in range(0, 3*n):
            for j in range(0, 3*m):
                f.write(str(s[i][j]) + " ")
            f.write("\n")

def DrawLayer(fig,resh,fl):
    ax = fig.add_subplot(1, 2, fl, projection = '3d')
    xval = np.linspace(0, 1, 3*N)
    yval = np.linspace(0, 1, 3*M)
    x, y = np.meshgrid(xval, yval)
    z = resh
    surf = ax.plot_surface(x, y, z, rstride = 1, cstride = 1, cmap = 'inferno')

def Draw(r1, r2):
    fig = plt.figure(figsize=(13, 5))
    DrawLayer(fig, r1, 1)
    plt.title("Вычисленный слой "+str(tau))
    DrawLayer(fig, r2, 2)
    plt.title("Истинный слой "+str(tau))
    plt.show()

N = 10
M = 10
#tau зависит от M и N как 2*(3M)^2 + 2*(3N)^2
tau = 3600
slo = sloi(N, M, tau)
to = tochn(N, M, tau)
k = 100
while k <= 1100:
    Draw(slo[k - 1], to[k - 1], k)
    k = k + 200
rn = rung(slo, N, M, tau)
ra = pogr(slo, to, N, M, tau)
print(rn)
print(ra)
save_in_file("tst.txt", slo[tau - 1], N, M)

