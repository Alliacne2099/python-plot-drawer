class Polynome:
	
	def __init__(self, source="1x0"):
		self.terms = []
		if type(source) is str:
			for piece in source.split("+"):
				self.add(piece)
		if isinstance(source, Polynome):
			for piece in source.terms:
				self.add(piece)

	def copy(self):
		return Polynome(self)

	def __repr__(self):
		return self.__str__()
	def __str__(self):
		self.terms.sort(key = lambda x:x[1], reverse = True)
		self.terms = list(filter(lambda x:x[0]!=0, self.terms))
		return " + ".join(map(lambda x: f"{x[0]}x^{x[1]}",  self.terms))

	def add(self, term):
		if type(term) is str:
			if term == "":
				return
			term = term.strip(" \n\r\t()").split("x^")
			if len(term) == 1:
				term = term[0].split("x")
			term = [float(term[0]),int(term[1])]
		if term[0] == 0:
			return
		# print(f"terms: {len(self.terms)}")
		ex = next(filter(lambda x:x[1]==term[1], self.terms), False)
		# print(f"ex: {ex}, {term}")
		if ex:
			ex[0] += float(term[0])
		else:
			self.terms.append(term.copy())
		pass
	
	def __add__(self, poly):
		if type(poly) is str:
			poly = Polynome(str)
		obj = self.copy()
		for term in self.terms:
			obj.add(term)
		return obj
	
	def __mul__(self, poly):
		if type(poly) is str:
			poly = Polynome(poly)
		if type(poly) == list:
			obj = self.copy()
			for term in obj.terms:
				term[0] *= poly[0]
				term[1] += poly[1]
				return obj
		if isinstance(poly, Polynome):
			obj = Polynome("")
			for term in self.terms:
				for term2 in poly.terms:
					obj.add([term[0]*term2[0],term[1]+term2[1]])
			return obj
			

class Fraction:
	def __init__(self, source="1x0", source2=False):
		self.num = Polynome("1x^0")
		self.div = Polynome("1x^0")
		if isinstance(source, Polynome):
			self.num = source.copy()
			self.div = source2.copy()
		if type(source) is str:
			if "/" in source:
				source, source2 = source.split("/")
			self.num = Polynome(source)
			self.div = Polynome(source2)
	
	def __mul__(self, frac):
		return Fraction(self.num * frac.num, self.div * frac.div)

	def __str__(self):
		return f"({self.num})/({self.div})"
	def __repr__(self):
		return self.__str__()




import sys
print("Рациональная дробь на основе списка (умножение)")

file = open("frac.txt", "r")
filelines = file.readlines()

fracs = {}

print("File values:")
for fileline in filelines:
	name, line = fileline.split(":")
	frac = Fraction(line)
	fracs[name.strip(" \n\r\t()")] = frac
	print(f"   {name} : {frac}")

fracn= 0
while True:
	todo = sys.stdin.readline()
	if todo.strip(" \n\r\t()") == "":
		break
	todo = todo.split("*")
	frac = False
	for nextFrac in todo:
		nextFrac = nextFrac.strip(" \n\r\t()")
		if not ("x" in nextFrac):
			if not (nextFrac in fracs):
				print(f"entry not found: {nextFrac}")
				break
			nextFrac = fracs[nextFrac]
		else:
			nextFrac = Fraction(nextFrac)
		if frac:
			frac = frac * nextFrac
		else:
			frac = nextFrac
		fracn += 1
		fracs[f"t{fracn}"] = frac
	print(f"t{fracn} = {frac}")
