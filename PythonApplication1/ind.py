import sys # Для sys.exit()
import numpy as np
import matplotlib.pyplot as plt
import keras
#
def loadBinData(pathToData, img_rows, img_cols, num_classes, show_img, useTestData):
    print('Загрузка данных из двоичных файлов...')
    with open(pathToData + 'imagesTrain.bin', 'rb') as read_binary:
        x_train = np.fromfile(read_binary, dtype = np.uint8)
    with open(pathToData + 'labelsTrain.bin', 'rb') as read_binary:
        y_train = np.fromfile(read_binary, dtype = np.uint8)
    with open(pathToData + 'imagesTest.bin', 'rb') as read_binary:
        x_test = np.fromfile(read_binary, dtype = np.uint8)
    with open(pathToData + 'labelsTest.bin', 'rb') as read_binary:
        y_test = np.fromfile(read_binary, dtype = np.uint8)
#
    print(x_train.shape) # (47040000,) # uncommented
    print(y_train.shape) # (60000,) # uncommented
    x_train_shape = int(x_train.shape[0] / (img_rows * img_cols)) # 60000
    x_test_shape = int(x_test.shape[0] / (img_rows * img_cols)) # 10000
    x_train = x_train.reshape(x_train_shape, img_rows, img_cols, 1)
    x_test = x_test.reshape(x_test_shape, img_rows, img_cols, 1)
    print(x_train.shape) # (60'000, 28, 28, 1) # uncommented
    print(y_train.shape) # (60'000,) # uncommented
    print(x_test.shape) # (10'000, 28, 28, 1) # uncommented
    print(y_test.shape) # (10'000,) # uncommented
    if show_img:
        if useTestData:
            print('Показываем примеры тестовых данных')
        else:
            print('Показываем примеры обучающих данных')
        # Выводим 9 изображений обучающего или тестового набора
        names = []
        for i in range(10): names.append(chr(48 + i)) # ['0', '1', '2', ..., '9']
        for i in range(9):
            plt.subplot(3, 3, i + 1)
            ind = y_test[i] if useTestData else y_train[i]
            img = x_test[i] if useTestData else x_train[i]
            img = img[:, :, 0]
            plt.imshow(img, cmap = plt.get_cmap('gray'))
            plt.title(names[ind])
            plt.axis('off')
        plt.subplots_adjust(hspace = 0.5) # wspace
        plt.show()
    # Преобразование целочисленных данных в float32 и приведение к диапазону
    # [0.0, 1.0]
    x_train = np.asarray(x_train, dtype = 'float32') / 255
    x_test = np.asarray(x_test, dtype = 'float32') / 255
    # Преобразование в бинарное представление: метки - числа из диапазона [0,
    # 9] в двоичный вектор размера num_classes
    # Так, в случае MNIST метка 5 (соответствует классу 6) будет преобразована
    # в вектор [0.  0.  0.  0.  0.  0.  1.  0.  0.  0.]
    #print(y_train[0]) # (MNIST) Напечатает: 5
    print('Преобразуем массивы меток в категориальное представление')
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)
    #print(y_train[0]) # (MNIST) Напечатает: [0.  0.  0.  0.  0.  1.  0.  0.
    #0.  0.]
    return x_train, y_train, x_test, y_test
#
pathToData = 'D:/Public/python/mnistBinData/'
img_rows = img_cols = 28
num_classes = 10
show_img = False
useTestData = False # True False
#
x_train, y_train, x_test, y_test = loadBinData(pathToData, img_rows, img_cols, num_classes, show_img, useTestData)
#
from keras.models import Model
from keras.layers import Input,Dense,Flatten
#input_shape = (28,28,1)
#inp = Input(shape = input_shape)
#x = Flatten()(inp)
#x = Dense(units = 256, activation = 'relu')(x)
#output = Dense(units=10,activation='softmax')(x)
#model = Model(inputs=inp, outputs=output)
#model.summary()
##model.compile(optimizer='Adam',loss='mse',metrics=['accuracy'])
##history = model.fit(x_train, y_train, batch_size=256, epochs = 10, verbose=2,
##validation_data=(x_test,y_test))







from keras.layers import Dropout, BatchNormalization

img_rows = img_cols = 28
num_classes = 10
epochs = 120
units = [0, 256, 128] # Если 0, то слоя Dense нет
rate = [0, 0, 0.5] # Если 0, то слоя Dropout нет
bn = [True, True, False] # Если False, то слоя BatchNormalization нет
input_shape = (img_rows, img_cols, 1)
inp = Input(shape = input_shape)
x = Flatten()(inp)
for k in range(3):
    if units[k] > 0:
        if rate[k] > 0:
            x = Dropout(rate[k])(x)
        x = Dense(units = units[k], activation = 'relu')(x)
        if bn[k]: x = BatchNormalization()(x)
output = Dense(num_classes, activation = 'softmax')(x)
model = Model(inputs = inp, outputs = output)


model.compile(optimizer='Adam',loss='mse',metrics=['accuracy']) # added
model.summary() # added
#
n_model = 'model' + str(epochs) + str(units) + str(rate) + str(bn) # Номер модели (указать состав и параметры модели)
print(n_model) # added
#
history = model.fit(x_train, y_train, batch_size=256, epochs = epochs, verbose=2, validation_data=(x_test,y_test)) #added
#
history = history.history
pathToHistory = 'D:/Public/python/mnistBinData/'
suff = str(n_model) + '.txt'
fn_loss = pathToHistory + 'loss_' + suff
fn_acc = pathToHistory + 'acc_' + suff
fn_val_loss = pathToHistory + 'val_loss_' + suff
fn_val_acc = pathToHistory + 'val_acc_' + suff
with open(fn_loss, 'w') as output:
    for val in history['loss']: output.write(str(val) + '\n')
with open(fn_acc, 'w') as output:
    for val in history['acc']: output.write(str(val) + '\n')
with open(fn_val_loss, 'w') as output:
    for val in history['val_loss']: output.write(str(val) + '\n')
with open(fn_val_acc, 'w') as output:
    for val in history['val_acc']: output.write(str(val) + '\n')
#
def one_plot(n, y_lb, loss_acc, val_loss_acc):
    plt.subplot(1, 2, n)
    if n == 1:
        lb, lb2 = 'loss', 'val_loss'
        yMin = 0
        yMax = 1.05 * max(max(loss_acc), max(val_loss_acc))
    else:
        lb, lb2 = 'acc', 'val_acc'
        yMin = min(min(loss_acc), min(val_loss_acc))
        yMax = 1.0
    plt.plot(loss_acc, color = 'r', label = lb, linestyle = '--')
    plt.plot(val_loss_acc, color = 'g', label = lb2)
    plt.ylabel(y_lb)
    plt.xlabel('Эпоха')
    plt.ylim([0.95 * yMin, yMax])
    plt.legend()
plt.figure(figsize = (9, 4))
plt.subplots_adjust(wspace = 0.5)
one_plot(1, 'Потери', history['loss'], history['val_loss'])
one_plot(2, 'Точность', history['acc'], history['val_acc'])
plt.suptitle('Потери и точность')
plt.show()
