
import numpy as np
import math
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

def save_in_file(filename, s,n, m):
	with open(filename, 'w') as f:
		for i in range(0, 3 * n):
			for j in range(0, 3 * m):
				f.write(str(s[j][i]) + " ")
			f.write("\n")

def DrawLayer(fig, resh, fl, num):
    plot = fig.add_subplot(1, num, fl, projection = '3d')
    xval = np.linspace(xi(0), xi(Nmax), Nmax)
    yval = np.linspace(yi(0), yi(Mmax), Mmax)
    x, y = np.meshgrid(xval, yval)
    z = resh
    surf = plot.plot_surface(x, y, z, rstride = 1, cstride = 1, cmap = 'inferno')


def Draw(*r):
    fig = plt.figure(figsize=(13, 5))
    for i in range(0, len(r) // 2):
        DrawLayer(fig, r[2 * i], i + 1, len(r) // 2)
        plt.title(r[2 * i + 1])
    plt.show()



N = 30
M = 10

Nmax = 3 * N + 2
Mmax = 3 * M + 2

x0 = 0
y0 = 0
hx = 1 / N
hy = 1 / M

C0 = 1 / 2 * ((hx ** 2 * hy ** 2) / (hx ** 2 + hy ** 2))
C1 = 1 / 2 * ((hx ** 2) / (hx ** 2 + hy ** 2))
C2 = 1 / 2 * ((hy ** 2) / (hx ** 2 + hy ** 2))


def xi(i):
	return x0 + (i - 1) * hx
def yi(i):
	return y0 + (i - 1) * hy

#форма пластины
def makeForm():
	base = ((0,0,1),(1,1,1),(0,1,1))
	
	form = np.zeros((Mmax,Nmax))
	
	for i in range(0, Nmax):
		for j in range(0, Mmax):
			form[j][i] = 0

	for i in range(1, Nmax - 1):
		for j in range(1, Mmax - 1):
			form[j][i] = base[(i - 1) // N][(j - 1) // M]
	
	return form

#матрица g вне пластины f внутри пластины
def makeInit(form, f, g):
	mx = np.zeros((Mmax,Nmax))
	
	for i in range(0, Nmax):
		for j in range(0, Mmax):
			if form[j][i] == 1:
				mx[j][i] = f(xi(i),yi(j))
			else:
				mx[j][i] = g(xi(i),yi(j))
	return mx


#нахождение решения по методу простой итерации
def makeStep(prev, form, vals):
	mx = np.zeros((Mmax,Nmax))
	
	for i in range(0, Nmax):
		for j in range(0, Mmax):
			if form[j][i] == 1:
				mx[j][i] = C0 * vals[j][i] + C1 * (prev[j - 1][i] + prev[j + 1][i]) + C2 * (prev[j][i - 1] + prev[j][i + 1])
			else:
				mx[j][i] = vals[j][i]
	return mx


#тестовая функция температуры
def u(x,y):
    if testN == 1:
        return math.sin(x) * math.cos(y)

    if testN == 2:
        return - 0.02 * y**2 + 0.003*x**2
    if testN == 3:
        return math.cos(x+y) 

    if testN == 4:
        return math.exp(x)+math.exp(y)

    return 

#граничная функция
def g(x,y):
	if testN > 0:
		return u(x,y)
	
	return 0

#распределение температуры внутри
def f(x, y):
    if testN == 1:
        return 2 * math.sin(x) * math.cos(y)
    if testN == 2:
        return 0.034
    if testN == 3:
        return 2*math.cos(x+y)

    if testN == 4:
        return -math.exp(x)-math.exp(y)

    return 0


def uInForm(mxU, form):
    mx = np.zeros((Mmax,Nmax))
    for i in range(0, Nmax):
        for j in range(0, Mmax):
            if form[j][i] == 1:
                mx[j][i] = mxU[j][i]
    return mx

def fn0(x,y):
    return 0

#двойной интеграл методом правых прямоугольников
def twoIntegrate(mx, i0, j0, i1, j1, f):
	s = 0
	for i in range(i0, i1):
		for j in range(j0, j1):
			s = s + f(mx[j + 1][i + 1])
	return s

def rungeTwoIntegrate(mx, i0, j0, i1, j1, f):
	s = 0
	for i in range(i0, i1, 2):
		for j in range(j0, j1, 2):
			s = s + f(mx[j + 1][i + 1])
	return s

def sqr(x):
	return x * x
#рассчет энергии методом правых прямоугольников
def energy(mx, i, j):
	return twoIntegrate(mx, 1 + (i - 1) * N,  1 + (i - 1) * M,  1 + (i) * N,  1 + (i) * M,  sqr) * hx * hy





testN = 2
fm = makeForm()
m0 = makeInit(fm, f, g)
m1 = makeStep(fm, m0, m0)

m4 = m1

for i in range(10000):
	if i % 100 == 0:
		print(i)
	m4 = makeStep(m4, fm, m0)

m2 = m1
for i in range(1000):
	m3 = makeStep(m2, fm, m0)
	eps1 = np.absolute(m2-m3).max()
	eps2 = np.absolute(m2-m3).sum()
	print(f"step: {i*10:4}, eps1: {eps1:10.3g}, eps2: {eps2:10.3g}")
	m2 = m3
	for j in range(9):
		m3 = makeStep(m3, fm, m0)
	eps1 = np.absolute(m2-m3).max()
	eps2 = np.absolute(m2-m3).sum()
	eps3 = np.absolute(m4-m3).max()
	eps4 = np.absolute(m4-m3).sum()
	print(f"     in 10: eps1: {eps1:10.3g}, eps2: {eps2:10.3g},  eps3: {eps3:10.3g}, eps4: {eps4:10.3g},   ")
	m2 = m3

#вывод результата
Draw(makeInit(fm, f, fn0),"f(x,y) внутри пластины",  makeInit(fm, fn0, g),"g(x,y) граница пластины",  uInForm(m2,fm) ,"результат")
Draw(makeInit(fm, f, fn0),"f",  makeInit(fm, fn0, g),"g",  m2 ,"слияние границы и результата",    uInForm(m2,fm) ,"U")




a = ((energy(m2, 1, 1), energy(m2, 1, 2), energy(m2, 1, 3)),
	(energy(m2, 2, 1), energy(m2, 2, 2), energy(m2, 2, 3)),
	(energy(m2, 3, 1), energy(m2, 3, 2), energy(m2, 3, 3)),)
a

print(a)




